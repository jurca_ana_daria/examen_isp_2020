import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Ex2 {
    public static void main(String[] args) {
        new GUI();
    }
} // main

class GUI extends JFrame {
    JLabel jLabel1 = new JLabel("Text1");
    JTextField text1 = new JTextField();
    //JTextField.setEditable(true);

    JButton jButton = new JButton("Copiaza");

    JLabel jLabel2 = new JLabel("Text2");
    JTextField text2 = new JTextField();
    //JTextField.setEditable(false);


    GUI() {
        this.setTitle("Exercitiul 2");
        this.setSize(500, 300);
        init();
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        jLabel1.setBounds(20, 20, 80, 20);
        text1.setBounds(100, 20, 350, 20);

        jButton.setBounds(20, 50, 100, 20);
        jButton.addActionListener(new TratareButon());

        jLabel2.setBounds(20, 80, 80, 20);
        text2.setBounds(100, 80, 350, 20);

        this.add(jLabel1);
        this.add(text1);
        this.add(jButton);
        this.add(jLabel2);
        this.add(text2);
    }

    class TratareButon implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            String words = text1.getText();
            String words2=text1.getText();

            int nrWords = words.split("\\s+", 0).length;
            int nrWords2=words2.split("\\s+",0).length;
            for (int i = 0; i < words.length(); i++) {
                words2 = words;
            }
            text2.setText(words2+"");
        }
    }
}

